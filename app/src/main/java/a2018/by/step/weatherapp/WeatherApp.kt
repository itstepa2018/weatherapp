package a2018.by.step.weatherapp

import android.app.Application
import timber.log.Timber

class WeatherApp : Application(){
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}