package a2018.by.step.weatherapp.model

data class WeatherThreeHour(
    val temperature: Double,
    val temperatureMin: Double,
    val temperatureMax: Double,
    val pressure: Double,
    val seaLevel: Double,
    val humidity: Int,
    val temperatureK: Double
//"temp": 23.78,
//"temp_min": 23.13,
//"temp_max": 23.78,
//"pressure": 1016.5,
//"sea_level": 1016.5,
//"grnd_level": 1011.55,
//"humidity": 54,
//"temp_kf": 0.66
)

data class Cloud(
    val cloudiness: Int
//     "all": 0
)

data class WeatherIcon(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
//    "id": 800,
//    "main": "Clear",
//    "description": "clear sky",
//"icon": "01n"
)

data class Wind(
    val speed: Double,
    val direction: Double
//    "speed": 1.32,
//    "deg": 238.947
)



